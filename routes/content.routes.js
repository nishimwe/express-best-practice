const ContentController = require('../controllers/content.controller');


exports.routesConfig = function (app) {
    app.post('/content', [
        ContentController.insert
    ]);
    app.get('/content', [
       
        ContentController.list
    ]);
    app.get('/content/:contentId', [
        ContentController.getById
    ]);
    app.patch('/content/:contentId', [
        ContentController.patchById
    ]);
    app.delete('/content/:contentId', [
        ContentController.removeById
    ]);
};