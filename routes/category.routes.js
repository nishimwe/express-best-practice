const CategoryController = require('../controllers/category.controller');


exports.routesConfig = function (app) {
    /**
 * @swagger
 * /category:
 *   post:
 *     tags:
 *       - Category
 *     description: create new Category
 *     produces:
 *       - application/json
*      parameters:
*       - name: category
*         description: category object
*         in: body
*         required: true
*         schema:
*           $ref: '#/models/Category'
 *     responses:
 *       200:
 *         description: Success
 *         schema:
 *           $ref: '#/models/Category'
 */
    app.post('/category', [
        CategoryController.insert
    ]);
    app.get('/category', [
       
        CategoryController.list
    ]);
    app.get('/category/:categoryId', [
        CategoryController.getById
    ]);
    app.patch('/category/:categoryId', [
        CategoryController.patchById
    ]);
    app.delete('/category/:categoryId', [
        CategoryController.removeById
    ]);
};