const mongoose = require('../config/mongoose.config').mongoose;
const Schema = mongoose.Schema;

const categorySchema = new Schema({
    imageUri: String,
    name_default: String,
    name_fr: String,
    name_eng: String,

});

categorySchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
categorySchema.set('toJSON', {
    virtuals: true
});

categorySchema.findById = function (cb) {
    return this.model('category').find({ id: this.id }, cb);
};

const Category = mongoose.model('category', categorySchema);


exports.findByEmail = (email) => {
    return Category.find({ email: email });
};
exports.findById = (id) => {
    return Category.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createCategory = (categoryData) => {
    const category = new Category(categoryData);
    return category.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Category.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, category) {
                if (err) {
                    reject(err);
                } else {
                    resolve(category);
                }
            })
    });
};

exports.patchCategory = (id, categoryData) => {
    return new Promise((resolve, reject) => {
        Category.findById(id, function (err, category) {
            if (err) reject(err);
            for (let i in categoryData) {
                category[i] = categoryData[i];
            }
            category.save(function (err, updatedCategory) {
                if (err) return reject(err);
                resolve(updatedCategory);
            });
        });
    })

};

exports.removeById = (categoryId) => {
    return new Promise((resolve, reject) => {
        Category.remove({ _id: categoryId }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};


