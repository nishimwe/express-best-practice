const mongoose = require('../config/mongoose.config').mongoose;
const Schema = mongoose.Schema;

const contentSchema = new Schema({
    uri: String,
    name_default: String,
    name_fr: String,
    name_eng: String,
    paid: String,
    category:String,

});

contentSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

// Ensure virtual fields are serialised.
contentSchema.set('toJSON', {
    virtuals: true
});

contentSchema.findById = function (cb) {
    return this.model('content').find({ id: this.id }, cb);
};

const Content = mongoose.model('content', contentSchema);


exports.findByCategory = (category) => {
    return Content.find({ category: category });
};
exports.findById = (id) => {
    return Content.findById(id)
        .then((result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        });
};

exports.createContent = (contentData) => {
    const content = new Content(contentData);
    return content.save();
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Content.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, content) {
                if (err) {
                    reject(err);
                } else {
                    resolve(content);
                }
            })
    });
};

exports.patchContent = (id, contentData) => {
    return new Promise((resolve, reject) => {
        Content.findById(id, function (err, content) {
            if (err) reject(err);
            for (let i in contentData) {
                content[i] = contentData[i];
            }
            content.save(function (err, updatedContent) {
                if (err) return reject(err);
                resolve(updatedContent);
            });
        });
    })

};

exports.removeById = (contentId) => {
    return new Promise((resolve, reject) => {
        Content.remove({ _id: contentId }, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};


