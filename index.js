const express = require('express');
const router = express();
const app=require("./config/app.config")
const config = require('./config/env.config.js');

router.use(app);
// router.use(express.static(path.join(__dirname, 'public')));

router.listen(config.port, function () {
    console.log('app listening at port %s', config.port);
});

module.exports = router;
