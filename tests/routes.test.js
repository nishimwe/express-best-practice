const request = require('supertest');
const app = require('../index.js');

const mongoose = require('mongoose');

const mongoDB = {
    mongoose,
    connect: () => {
        mongoose.Promise = Promise;
        mongoose.connect("mongodb://localhost:27017/moapp");
    },
    disconnect: (done) => {
        mongoose.disconnect(done);
    },
};

describe('TestGET /content', () => {
    beforeAll(() => {
        mongoDB.connect();
    });

    afterAll((done) => {
        mongoDB.disconnect(done);
    });
    test('It should response the GET method', () => {
        request(app)
        .get('/content')
        .expect(200);

    });
});
describe('TestGET /category', () => {
    beforeAll(() => {
        mongoDB.connect();
    });

    afterAll((done) => {
        mongoDB.disconnect(done);
    });
    test('It should response the GET method', () => {
        request(app)
        .get('/category')
        .expect(200);

    });
});
