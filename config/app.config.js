const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var swaggerJSDoc = require('swagger-jsdoc');
const path=require('path');

const contentRouter = require('../routes/content.routes');
const categoryRouter = require('../routes/category.routes');
// const welcomeRouter = require("../routes/welcome.routes");

var swaggerDefinition = {
    info: {
      title: 'Test Swagger API',
      version: '1.0.0',
      description: 'Test how to describe a RESTful API with Swagger',
    },
    host: 'localhost:3600',
    basePath: '/',
  };
  
  // options for the swagger docs
  var options = {
    // import swaggerDefinitions
    swaggerDefinition: swaggerDefinition,
    // path to the API docs
    apis: ['../routes/*.js'],
  };
  
  // initialize swagger-jsdoc
  var swaggerSpec = swaggerJSDoc(options);



app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    if (req.method === 'OPTIONS') {
        return res.send(400);
    } else {
        return next();
    }
});

app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));

contentRouter.routesConfig(app);
categoryRouter.routesConfig(app);

// app.get('/', (req, res) => {
//     res.status(200).send('Hello World!')
// })

// serve swagger
app.get('/swagger.json', function(req, res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(swaggerSpec);
  });

module.exports=app