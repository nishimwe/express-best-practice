module.exports = {
    "port": 3600,
    "appEndpoint": "http://localhost:3600",
    "apiEndpoint": "http://localhost:3600",
    "paymentMethod": {
        "PAID_CONTENT": 1,
        "FREE_CONTENT": 4,
        "PROMOTION_CONTENT": 6
    }
};
